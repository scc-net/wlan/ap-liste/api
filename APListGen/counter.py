import re


def site_count(ap_dict):
    south = re.compile(r"^(was.*|ws[s\-\d].*).*$")  # Pattern matching was*, ws-*, wss* or ws$digit* but not wsn
    north = re.compile(r"^(wan.*|wn.*|wsn.*)$")  # Pattern matching wan*, wn* or wsn*
    n_south = 0
    n_north = 0
    n_misc = 0
    for key, obj in ap_dict.items():
        if south.match(obj.name):
            n_south += 1
        elif north.match(obj.name):
            n_north += 1
        else:
            n_misc += 1
    return {'cs': n_south, 'cn': n_north, 'misc': n_misc}


def vendor_count(ap_dict):
    aruba = re.compile(r"^aruba.*$", re.IGNORECASE)
    n_aruba = 0
    n_misc = 0
    for key, obj in ap_dict.items():
        if aruba.match(obj.nd_module_type):
            n_aruba += 1
        else:
            n_misc += 1
    return {'aruba': n_aruba, 'misc': n_misc}
