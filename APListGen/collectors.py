import aruba_api_caller
import requests
from requests.auth import HTTPBasicAuth


def get_pings(username, password):
    """
    Gets pings of accesspoints from icinga.tmn.scc.kit.edu

    :return: dict with fqdns as keys and ping status as values (strings)
    """
    ping_dict = {}
    status = False
    try:
        response = requests.get(
            "https://icinga.tmn.scc.kit.edu/cgi-bin/icinga/status.cgi",
            params={"hostgroup": "accesspoints", "style": "hostdetail", "jsonoutput": True},
            auth=HTTPBasicAuth(username=username, password=password),
            timeout=5
        )
        if response.status_code == 401:
            status = 401
            raise requests.exceptions.ConnectionError
        json_response = response.json()
        for host in json_response["status"]["host_status"]:
            ping_dict[host["host_name"]] = host["status"].lower()
        status = response.status_code
        return ping_dict, status
    except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
        return ping_dict, status


def get_mm_data(username, password):
    """
    Fetches serial-numbers of all aruba aps from aruba-mm

    :param username: username for api
    :param password: password for api
    :return: dict containing ap-names (keys) and sn's (values)
    """
    api = aruba_api_caller.APIClient(aruba_api_caller.APIConfiguration("aruba-mm.scc.kit.edu", username, password))

    response = api.cli_command("show ap database long")
    result = {
        item["Name"]: dict(
            serialnumber=item["Serial #"],
            mac=item["Wired MAC Address"]
        ) for item in response["AP Database"]
    }
    return result
