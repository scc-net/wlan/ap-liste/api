from pint.errors import UndefinedUnitError, DimensionalityError

from APListGen import ureg
from APListGen.switches import SwitchError, SwitchList


class AP(object):
    """
    Represents one AP
    """

    def __init__(self, name):
        self.name = name
        self.nd_module_type = None
        self.nd_device = False
        self.nd_device_fqdn = None
        self.nd_connection_switch = None
        self.nd_connection_switch_fqdn = None
        self.nd_connection_port = None
        self.nd_connection_type = None
        self.nd_connection_speed = None
        self.nd_wall_port = None
        self.snmp_connection_speed = None
        self.power_consumption = None
        self.icinga_ping = "-"
        self.arubaapi_mac = "-"
        self.arubaapi_serialnumber = "-"
        self.errors = {}

    def print_values(self):
        attrs = vars(self)
        for key, value in attrs.items():
            print(f"{key}: {value}")
        return

    def parse_ports(self, response):
        """
        Parses ND-Port dict and sets Connection Speed, Connected Wall-Port and eventual errors.

        :return:
        """
        port_gfk_list = None
        start_p_port = None
        nd_ap_connection_speed = None
        for port in response["start_p_port"]:
            if port["mdl_fq_name"] == self.name:
                if port["type_group"] == "Antennen/gerichtet":
                    return

                # parse AP Connection Speed, set to False if parse is not successful
                try:
                    nd_ap_connection_speed = int(ureg(port["speed"]).to('Mbit').magnitude)
                except UndefinedUnitError | DimensionalityError:
                    nd_ap_connection_speed = False

                port_gfk_list = port["connection_id_gfk_list"]
                start_p_port = port
                break

        nd_switch_connection_speed = None
        end_mdl_fq_name = None
        if port_gfk_list is not None:
            for port in response["end_p_port"]:
                if port["gpk"] in port_gfk_list:
                    end_mdl_fq_name = port["mdl_fq_name"]
                    self.nd_connection_port = port["name"]
                    self.nd_connection_type = port["type"]

                    # parse Switch Connection Speed, set to False if parse is not successful
                    try:
                        nd_switch_connection_speed = int(ureg(response["end_p_port"][0]["speed"]).to('Mbit').magnitude)
                    except UndefinedUnitError:
                        nd_switch_connection_speed = False
                    break

        end_mdl_gfk_list = None
        for module in response["end_module"]:
            if module["fq_name"] == end_mdl_fq_name:
                end_mdl_gfk_list = module["module_hierarchy_gfk_list"]

        if end_mdl_gfk_list:
            for module in response["parent_module"]:
                if module["gpk"] in end_mdl_gfk_list and module["is_active"] and not module["is_child"]:
                    self.nd_connection_switch = module["fq_name"]

        for m2d in response["end_m2d"]:
            if m2d["mdl_fq_name"] == self.nd_connection_switch:
                self.nd_connection_switch_fqdn = m2d["dev_fqdn"]

        # Get slowest connection speed of switch and AP documented in ND
        if nd_ap_connection_speed and nd_switch_connection_speed:
            self.nd_connection_speed = int(min(nd_ap_connection_speed, nd_switch_connection_speed))
        elif nd_ap_connection_speed:
            self.nd_connection_speed = int(nd_ap_connection_speed)
        elif nd_switch_connection_speed:
            self.nd_connection_speed = int(nd_switch_connection_speed)
        # Get Wall Port
        self.get_wall_port(start_p_port, end_mdl_fq_name is not None)
        if end_mdl_fq_name is None:
            self.errors["ND-Connection"] = "AP-Verkabelung nicht vollständig dokumentiert."
        return

    def get_snmp_data(self, switches: SwitchList):
        """
        Assigns all data queried via snmp

        :return:
        """
        try:
            if self.nd_connection_switch_fqdn is None:
                self.errors["SNMP"] = "No switch found to query"
                return

            self.snmp_connection_speed = switches.get_port_speed(self.nd_connection_switch_fqdn,
                                                                 self.nd_connection_port)
            if self.nd_connection_type == "RJ45-POE":
                self.power_consumption = switches.get_power_consumption(self.nd_connection_switch_fqdn,
                                                                        self.nd_connection_port)
            else:
                self.power_consumption = "-"
        except SwitchError as e:
            self.errors["SNMP"] = str(e)

    def get_wall_port(self, start_p_port, connected: bool):
        """
        Sets Wall Port for AP-Module.
        If AP is directly linked, "Direkt" will be set
        If no connection can be found "NC" gets set.

        :return:
        """
        if not connected:
            self.nd_wall_port = "NC"
            return

        if start_p_port["connected_name"] in ["L", "R", "M"]:
            # Variant: more information
            # self.nd_wall_port = (f'{start_p_port["connected_mdl_fq_name"]} ({start_p_port["connected_mdl_fq_name"]}) '
            #                      f'B{start_p_port["connected_mdl_bldg"]} R{start_p_port["connected_mdl_room"]}')
            # Old Variant:
            self.nd_wall_port = f'{start_p_port["connected_mdl_fq_name"]}'
        else:
            self.nd_wall_port = "Direkt"
