import re
from re import Match

from netdb_client.api41 import APISession
from prometheus_api_client import PrometheusConnect


class SwitchError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return f"Switch error: {self.message}."
        else:
            return f"Switch error."


class Switch(object):
    """
    Represents one Switch
    """

    def __init__(self, fqdn, mapping_dict, prometheus_host: str):
        self.error = ""
        self.fqdn = fqdn[:-1]
        self.vendor = mapping_dict["vendor"]
        self.has_poe_mib = False
        if re.match(r"^X440-G2-\d\dp", mapping_dict["type"]) or self.vendor in ["cisco"]:
            self.has_poe_mib = True
        self.prometheus = PrometheusConnect(url=prometheus_host)
        self._get_prometheus_data()

    def _get_prometheus_data(self):
        # TODO: Maybe rewrite as one big query
        speeds = {}
        poe = {}

        query = 'ifHighSpeed{{instance=~"{}"}}'.format(self.fqdn)
        result_speeds = self.prometheus.custom_query(query)

        for result in result_speeds:
            port = port_sub(result['metric']['ifName'])
            if check_port(port):
                speeds[port] = {'speed': result['value'][1]}
            else:
                pass

        if self.vendor == "cisco":
            result_poe = self.prometheus.custom_query('cpeExtPsePortPwrConsumption{{instance=~"{}"}}'.format(self.fqdn))

            for result in result_poe:
                # This port hack is ugly. Ask cisco for better port naming.
                port = "GigabitEthernet" + result['metric']['pethPsePortGroupIndex'] + "/0/" + result['metric'][
                    'pethPsePortIndex']
                if check_port(port):
                    poe[port] = {'power': result['value'][1]}
                else:
                    pass
        elif self.vendor == "extreme":
            result_poe = self.prometheus.custom_query(
                'extremePethPortMeasuredPower{{instance=~"{}"}}'.format(self.fqdn))

            for result in result_poe:
                port_group = result['metric']['pethPsePortGroupIndex']
                port_number = result['metric']['pethPsePortIndex']
                if re.match(r"1\d{3}", port_number):
                    port_number = str(int(port_number[1:4]))
                port = port_group + ":" + port_number
                if check_port(port):
                    poe[port] = {'power': result['value'][1]}
                else:
                    pass

        # merge the dicts by key
        self.ports = {}
        for port in speeds:
            self.ports[port] = {**speeds[port]}
            if port in poe:
                self.ports[port] = {**self.ports[port], **poe[port]}
            else:
                pass


def port_sub(port: str) -> str:
    # Cisco Naming Scheme
    cisco_speed_types = [("Fa", "FastEthernet"),  # 100 Mbit/s
                         ("Gi", "GigabitEthernet"),  # 10/100/1000 Mbit/s
                         ("Tw", "TwoGigabitEthernet"),  # 2.5 Gbit/s
                         ("Fi", "FiveGigabitEthernet"),  # 5 Gbit/s
                         ("Te", "TenGigabitEthernet"),  # 10 Gbit/s
                         ("Twe", "TwentyFiveGigE"),  # 25 Gbit/s
                         ("Fo", "FortyGigabitEthernet")  # 40 Gbit/s
                         ]
    for speed_type in cisco_speed_types:
        port = re.sub(speed_type[0] + r"(\d+)/(\d+)/(\d+)", speed_type[1] + r"\1/\2/\3", port)
        port = re.sub(speed_type[0] + r"(\d+)/(\d+)", speed_type[1] + r"\1/\2", port)

    # Transform port names like 1001 into 1:1 (Extreme)
    if re.match(r"\d{4}", port):
        stack = int(port[0])
        port_number = int(port[1:3])
        port = str(stack) + str(port_number)
    return port


def check_port(port: str) -> bool:
    cisco_speed_types = ["FastEthernet", "GigabitEthernet", "TwoGigabitEthernet", "FiveGigabitEthernet",
                         "TenGigabitEthernet", "TwentyFiveGigE", "FortyGigabitEthernet"]
    patterns = [r"ge.\d+.\d+", r"tg.\d+.\d+", r"fe.\d+.\d+",  # ?
                r"\d+:\d+"]  # extreme
    for speed_type in cisco_speed_types:
        patterns.append(speed_type + r"\d+/\d+/\d+")
        patterns.append(speed_type + r"\d+/\d+")
    for pattern in patterns:
        if re.match(pattern, port):
            return True
    return False


class SwitchList(object):
    def __init__(self, netdb: APISession, prometheus_host: str):
        self.switch_dict = {}
        self.prometheus_host = prometheus_host
        response = netdb.execute_ta([
            {"idx": "device", "name": "nd.device.list", "old": {"type": "a"}},
            {"idx": "fqdn", "name": "dns.fqdn.list", "inner_join_ref": {"device": "default"}},
            {"idx": "m2d", "name": "nd.module2device.list", "inner_join_ref": {"fqdn": "default"}},
            {"idx": "module", "name": "nd.module.list", "inner_join_ref": {"m2d": "default"},
             "old": {"is_child": False}}
        ], dict_mode=True)
        self.mapping = {}
        for device in response["device"]:
            mdl_fq_names = []
            for m2d in response["m2d"]:
                if m2d["dev_fqdn"] == device["fqdn"]:
                    mdl_fq_names.append(m2d["mdl_fq_name"])

            if len(mdl_fq_names) == 0:
                raise Exception(f"No module found for {device['fqdn']}")

            module = None
            for mod in response["module"]:
                if mod["fq_name"] in mdl_fq_names:
                    module = mod
                    break
            if module is None:
                continue

            tmp = {
                "fqdn": device["fqdn"],
                "type": module["type"]
            }
            # Detecting switch vendor
            if re.match(r"cisco", module['type'], flags=re.IGNORECASE):
                vendor = "cisco"
            elif re.match(r"extreme", module['type'], flags=re.IGNORECASE):
                vendor = "extreme"
            else:
                vendor = "other"

            tmp["vendor"] = vendor
            self.mapping[device["fqdn"]] = tmp

    def _get_attribute(self, fqdn, attribute):
        """
        Get attribute value from switch object. If object does not exist it gets created and populated.

        :param name: switch's module name
        :param attribute: name of the attribute
        :return: value, error-flag
        """
        if fqdn not in self.mapping:
            raise SwitchError("Device not in database")

        if fqdn not in self.switch_dict:
            self.switch_dict[fqdn] = Switch(fqdn, self.mapping[fqdn], self.prometheus_host)
        if self.switch_dict[fqdn].error:
            raise SwitchError(self.switch_dict[fqdn].error)
        else:
            return getattr(self.switch_dict[fqdn], attribute)

    def get_vendor(self, switch_fqdn):
        """
        Returns vendor and error flag in tuple

        :param switch_fqdn: switch's module name
        :return: tuple (vendor, error-flag)
        """
        return self._get_attribute(switch_fqdn, "vendor")

    def get_port_speed(self, switch_fqdn, port):
        """
        Gets port speed from snmp data

        :param switch_fqdn: switch's fqdn
        :param port: port name (ND)
        :return: tuple (speed, error-flag)
        """
        ports = self._get_attribute(switch_fqdn, "ports")
        try:
            return ports[port]["speed"]
        except KeyError:
            return "?"

    def get_power_consumption(self, switch_fqdn, port):
        """
        Returns power output for POE-Capable Switches
        Switch musst support delivering this data via SNMP at the moment
        only Extreme X440-G2 POE Switches are capable of this.

        :param switch_fqdn:
        :param port:
        :return:
        """
        ports = self._get_attribute(switch_fqdn, "ports")
        try:
            return ports[port]["power"]
        except KeyError:
            return "?"
