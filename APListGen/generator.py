import os
import re
import time
from collections import Counter

import requests
from netdb_client.api41 import APISession

from APListGen.aps import AP
from APListGen.collectors import get_mm_data, get_pings
from APListGen.counter import site_count, vendor_count
from APListGen.switches import SwitchList


def generate_data(netdb: APISession):
    # Get ap-data from netdoc
    try:
        response = netdb.execute_ta([
            {"idx": "module_type_class", "name": "ndcfg.module_type_class.list", "old": {"name": "FuAP"}},
            {"idx": "module_type", "name": "ndcfg.module_type.list",
             "inner_join_ref": {"module_type_class": "default"}},

            {"idx": "module", "name": "nd.module.list", "inner_join_ref": {"module_type": "default"}},

            {"idx": "start_p_port", "name": "nd.p_port.list",
             "inner_join_ref": {"module": "default"},
             "old": {"is_connected": True}},

            {"idx": "end_p_port", "name": "nd.p_port.list",
             "inner_join_ref": {"start_p_port": "api_fkey_nd_p_port_conn_dest"},
             "old": {"type_group": "Kupfer/ETH/STANDARD", "is_connected": True, "is_edge_node": True}},
            {"idx": "end_module", "name": "nd.module.list", "inner_join_ref": {"end_p_port": "default"}},
            {"idx": "end_m2d", "name": "nd.module2device.list", "inner_join_ref": {"end_module": "default"}},

            {"idx": "parent_module", "name": "nd.module.list",
             "inner_join_ref": {"end_module": "api_func_nd_module_hierarchy_gfk_list_is_subset"}},

            {"idx": "end_p_port_wall", "name": "nd.p_port.list",
             "inner_join_ref": {"start_p_port": "api_fkey_nd_p_port_conn_dest"},
             "old": {"is_edge_node": False, "type_group": "Kupfer/ETH/STANDARD"}},
            {"idx": "module_wall", "name": "nd.module.list", "inner_join_ref": {"end_p_port_wall": "default"}},

            {"idx": "m2d", "name": "nd.module2device.list", "inner_join_ref": {"module": "default"}}
        ], dict_mode=True)
    except requests.exceptions.ConnectionError:
        raise ConnectionError
    ap_dict = {}
    # Query AP Status from Icinga
    ping_dict, ping_status = get_pings(os.getenv("ICINGA_USER", "username"),
                                       os.getenv("ICINGA_PW", "password"))
    # Query Aruba API Data from MM
    api_data = get_mm_data(os.getenv("ARUBA_USER", "username"), os.getenv("ARUBA_PW", "password"))

    switch_list = SwitchList(netdb, os.getenv("PROMETHEUS_HOST"))

    # parse ap-data from netdoc
    for module in response["module"]:
        ap_name = module["fq_name"]
        # Create new AP entry
        ap_dict[ap_name] = AP(ap_name)
        # set AP-Type
        ap_dict[ap_name].nd_module_type = module["type"]

        if len(response["m2d"]) > 0:
            # get FQDN of AP device (strip last dot)
            fqdn = None
            for m2d in response["m2d"]:
                if m2d["mdl_fq_name"] == module["fq_name"]:
                    fqdn = m2d["dev_fqdn"]
                    break

            if fqdn is None:
                ap_dict[ap_name].errors["ND-Device"] = f"Device für Module ({ap_name}) nicht gefunden"
            else:
                ap_dict[ap_name].nd_device_fqdn = fqdn.rstrip(".")

                # Set ND-Device status
                ap_dict[ap_name].nd_device = True
                # Add error if module and device name differs
                if ap_dict[ap_name].nd_device_fqdn.split(".")[0] != ap_name:
                    ap_dict[ap_name].errors["ND-Device"] = \
                        f"Modul ({ap_name}) und Device ({ap_dict[ap_name].nd_device_fqdn}) haben unterschiedliche Namen."
                # Check for AP-Status in Icinga-Query
                if ping_status == 200:
                    try:
                        ap_dict[ap_name].icinga_ping = ping_dict[ap_dict[ap_name].nd_device_fqdn]
                    except KeyError:
                        pass
                else:
                    ap_dict[ap_name].icinga_ping = ping_status
                # If vendor is aruba, check for MM-API Data
                if re.match(r"^aruba.*$", ap_dict[ap_name].nd_module_type):
                    try:
                        ap_dict[ap_name].arubaapi_mac = api_data[ap_dict[ap_name].nd_device_fqdn.split(".")[0]]["mac"]
                        ap_dict[ap_name].arubaapi_serialnumber = \
                            api_data[ap_dict[ap_name].nd_device_fqdn.split(".")[0]]["serialnumber"]
                    except KeyError:
                        pass

        ap_dict[ap_name].parse_ports(response)
        if len(response["parent_module"]) > 0 and response["parent_module"][0]["is_active"]:
            ap_dict[ap_name].get_snmp_data(switch_list)

    data = {
        "data": [ap_obj.__dict__ for ap_obj in ap_dict.values()],
        "counters": {
            'site_counters': site_count(ap_dict),
            'vendor_counters': vendor_count(ap_dict),
            'ping_counters': Counter([obj.icinga_ping for obj in ap_dict.values()]),
            'error_counter': Counter([True for obj in ap_dict.values() if obj.errors])[True],
            'speed_counter': Counter([True for obj in ap_dict.values() if
                                      obj.snmp_connection_speed is not None and "SNMP" not in obj.errors and
                                      obj.snmp_connection_speed != obj.nd_connection_speed])[True]
        },
        "timestamp": time.time()
    }
    return data
