# AP-Liste

Rewrite of the old wifi-ap-list-generator (php) in python.
The website is generated and served by flask. The data is collected with the script `generateList.py` and gets stored in the file `output.json`.

## Runing
### Webserver
Config is loaded via environment Variables. Variables that should be set in **production**:
 * `FLASK_ENV` set to `production`, for dev set it to `development`
 * `FLASK_DEBUG` set to `1` or `0` 
 * `FLASK_SECRET_KEY` set to random string, generate it with `python -c 'import os; print(os.urandom(16))'`
 * Optional: `STOREFILE` filepath of the cache-file for `generateList.py` and Flask App. Defaults to `output.json`.

Run app with `python3 run.py` or via uwsgi or similar.

### Generate List
I recommend using a systemd-timerd (or cron) job to periodically regenerate the list.

Used Environment-Variables are here:
 * `ICINGA_USER` and `ICINGA_PW`: Credentials for [net-icinga](https://icinga.tmn.scc.kit.edu/)
 * `ARUBA_USER` and `ARUBA_PW`: Credentials for [Aruba MM-API](https://aruba-mm.scc.kit.edu/api/)
 * `STOREFILE` _(Optional)_: filepath of the cache-file for `generateList.py` and Flask App. Defaults to `output.json`.
 
 Run `python3 generateList.py`.

## Dependencies

### Production
 * python >= 3.7
 * nodejs / npm

For python-package dependencies see `pyproject` and `poetry.lock`.
For deployment is an synced `requirements.txt`
If you have installed poetry, install the Dependencies with `poetry install`

Run `npm install` in `APListe/static` to install web-dependecies.