#!/usr/bin/env python3

import json
import logging
import os

from netdb_client.api41 import APISession, APIEndpoint
from netdb_client.util import ArgumentParser

from APListGen.generator import generate_data

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    parser = ArgumentParser(prog='aruba_ap_device_importer')
    netdb = APISession(APIEndpoint(**vars(parser.parse_args())))
    data = generate_data(netdb)
    exportfile = os.getenv("STOREFILE", "output.json")
    with open(exportfile, "w") as outfile:
        json.dump(data, outfile)
