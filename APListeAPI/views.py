import json
import os

from flask import jsonify

from APListeAPI import app
from . import helpers

STOREFILE = os.getenv("STOREFILE", "output.json")


@app.route("/", methods=["GET"])
def home():
    return "API Namespace"


@app.route("/aplist", methods=["GET"])
def get_list():
    with open(STOREFILE, "r") as infile:
        return jsonify(json.load(infile)['data'])


@app.route("/counters", methods=["GET"])
def get_counters():
    with open(STOREFILE, "r") as infile:
        return jsonify(json.load(infile)["counters"])


@app.route("/counters/site", methods=["GET"])
def get_site_counters():
    with open(STOREFILE, "r") as infile:
        return jsonify(json.load(infile)['counters']['site_counters'])


@app.route("/counters/vendor", methods=["GET"])
def get_vendor_counters():
    with open(STOREFILE, "r") as infile:
        return jsonify(json.load(infile)['counters']['vendor_counters'])


@app.route("/counters/ping", methods=["GET"])
def get_ping_counters():
    with open(STOREFILE, "r") as infile:
        return jsonify(json.load(infile)['counters']['ping_counters'])


@app.route("/counters/error", methods=["GET"])
def get_error_counter():
    with open(STOREFILE, "r") as infile:
        return jsonify(json.load(infile)['counters']['error_counter'])


@app.route("/timestamp", methods=["GET"])
def get_timestamp():
    with open(STOREFILE, "r") as infile:
        return jsonify(json.load(infile)['timestamp'])


@app.route("/version", methods=["GET"])
def get_version():
    return jsonify(helpers.get_version())
