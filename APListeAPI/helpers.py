import git


def get_version():
    repo = git.Repo(search_parent_directories=True)
    try:
        version = str([tag for tag in repo.tags if tag.commit == repo.head.commit][0])
    except (IndexError, ValueError):
        version = str(repo.head.object.hexsha)
    return version
