import os

from flask import Flask


def build_app():
    application = Flask(__name__, instance_relative_config=True)
    if application.config['DEBUG']:
        from flask_cors import CORS
        CORS(application)
    application.config['SECRET_KEY'] = os.getenv("FLASK_SECRET_KEY", "dev")
    return application


app = build_app()

import APListeAPI.views
